android-platform-libcore (10.0.0+r36-1) unstable; urgency=medium

  [ Raman Sarda ]
  * Team upload.
  * New upstream version.
  * Use debhelper-compat (= 12).
  * Bump Standards version to 4.5.0.
  * Updated source and target compatibility to 1.8 in debian/build.gradle.
  * Add patches to remove the following annotations from json/:
    - UnsupportedAppUsage
    - NonNull
    - Nullable
    - CorePlatformApi

  [ Andrej Shadura ]
  * Includes moved to luni/src/main/native, update the paths.

 -- Andrej Shadura <andrewsh@debian.org>  Fri, 22 May 2020 11:19:20 +0200

android-platform-libcore (8.1.0+r23-2) unstable; urgency=medium

  * Upload to unstable
  * Standards-Version => 4.2.1

 -- Kai-Chung Yan <seamlikok@gmail.com>  Tue, 09 Oct 2018 14:49:12 +0800

android-platform-libcore (8.1.0+r23-1) experimental; urgency=medium

  * New upstream release (Closes: #883894, #898705)
  * Remove the Javadoc package due to low popcon

  [ 殷啟聰 | Kai-Chung Yan ]
  * Upgrade to debhelper 11
  * Install the `NOTICE` file in every package
  * Remove `libandroid-dex-java`: Sources are moved to `android-platform-dalvik`

 -- Saif Abdul Cassim <saif.15@cse.mrt.ac.lk>  Wed, 30 May 2018 14:15:35 +0530

android-platform-libcore (7.0.0+r33-1) unstable; urgency=medium

  * New upstream release

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 24 Apr 2017 20:24:35 +0200

android-platform-libcore (7.0.0+r3-1) unstable; urgency=medium

  * Upload to unstable
  * New upstream release
    * No changes but as I re-imported 7.0.0+r1 the orig-tar conflicts with the
      archive.
  * Let gradle-debian-helper generate Maven POMs
    * d/control: Build-Depends on gradle-debian-helper (>= 1.4~)

 -- Kai-Chung Yan <seamlikok@gmail.com>  Tue, 06 Dec 2016 18:23:08 +0800

android-platform-libcore (7.0.0+r1-3) experimental; urgency=medium

  * debian/copyright: Mention GPL-2 licensed files in ojluni
  * debian/copyright: Exclude *.dex
  * Re-import 7.0.0+r1 to exclude *.dex

 -- Kai-Chung Yan <seamlikok@gmail.com>  Tue, 11 Oct 2016 14:12:51 +0200

android-platform-libcore (7.0.0+r1-2) experimental; urgency=medium

  * Update to debhelper 10
  * New package: android-platform-libcore-headers

 -- Kai-Chung Yan <seamlikok@gmail.com>  Tue, 27 Sep 2016 15:46:38 +0800

android-platform-libcore (7.0.0+r1-1) experimental; urgency=medium

  * New upstream release
  * get-orig-source: Make use of d/watch
  * New package: libandroid-json-java-doc
  * No longer provide libandroid-luni-java: No one is using luni.jar and it is
    complicated to maintain
  * Remove transitional package libdex-java
  * d/generatePom.gradle: Sort the dependencies in the generated POMs for
    reproducibility
  * Build-Depends on default-jdk-headless | default-jdk

 -- Kai-Chung Yan <seamlikok@gmail.com>  Thu, 22 Sep 2016 17:03:51 +0800

android-platform-libcore (6.0.1+r55-1) unstable; urgency=medium

  [ Hans-Christoph Steiner ]
  * New upstream release to sync with all android-tools projects

  [ Kai-Chung Yan ]
  * Set JARs version to "android-6.0.1"
  * Add groupId to JAR filename (Upstream does not name them)
  * Rename package: libdex-java => libandroid-dex-java
  * d/control: Move all BD-Indep to BD since there is no arch packages
  * d/copyright: Refinements
  * New package: libandroid-luni-java
  * Remove Javadoc packages: Provided by libandroid-${version}-java-doc
  * Standards-Version => 3.9.8 (no changes)

  [ Chirayu Desai ]
  * New upstream release.
  * debian: Add watch file for usage with uscan
  * debian/watch: recompress using xz

 -- Kai-Chung Yan <seamlikok@gmail.com>  Tue, 26 Jul 2016 15:34:52 +0800

android-platform-libcore (6.0.1+r10-2) unstable; urgency=medium

  * d/control: Standards-Version => 3.9.7 (No change)
  * d/rules: Remove unused javahelper override
  * Use javahelper to install Javadoc
  * Fix Vcs-Git

 -- Kai-Chung Yan <seamlikok@gmail.com>  Thu, 04 Feb 2016 21:40:03 +0800

android-platform-libcore (6.0.1+r10-1) unstable; urgency=low

  * Initial release (Closes: #795995)

 -- Kai-Chung Yan <seamlikok@gmail.com>  Fri, 15 Jan 2016 17:20:44 +0800
