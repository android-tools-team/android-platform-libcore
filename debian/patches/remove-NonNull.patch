Description: Remove NonNull annotation.
 NonNull annnotation is just a warning that hints when you pass null to
 methods that otherwise expect a non null arguments. Can be safely
 ignored since it otherwise fails the build as it comes from libcore itself,
 which we don't build.
Author: Raman Sarda <theloudspeaker@disroot.org>
Last-Update: 2020-05-18

--- a/json/src/main/java/org/json/JSONObject.java
+++ b/json/src/main/java/org/json/JSONObject.java
@@ -23,7 +23,6 @@ import java.util.LinkedHashMap;
 import java.util.Map;
 import java.util.Objects;
 import java.util.Set;
-import libcore.util.NonNull;
 import libcore.util.Nullable;
 
 // Note: this class was written without inspecting the non-free org.json sourcecode.
@@ -99,7 +98,7 @@ public class JSONObject {
      * returning true when compared to {@code null}. Its {@link #toString}
      * method returns "null".
      */
-    @NonNull public static final Object NULL = new Object() {
+    public static final Object NULL = new Object() {
         @Override public boolean equals(Object o) {
             return o == this || o == null; // API specifies this broken equals implementation
         }
@@ -128,7 +127,7 @@ public class JSONObject {
      * @throws NullPointerException if any of the map's keys are null.
      */
     /* (accept a raw type for API compatibility) */
-    public JSONObject(@NonNull Map copyFrom) {
+    public JSONObject(Map copyFrom) {
         this();
         Map<?, ?> contentsTyped = (Map<?, ?>) copyFrom;
         for (Map.Entry<?, ?> entry : contentsTyped.entrySet()) {
@@ -153,7 +152,7 @@ public class JSONObject {
      * @throws JSONException if the parse fails or doesn't yield a
      *     {@code JSONObject}.
      */
-    public JSONObject(@NonNull JSONTokener readFrom) throws JSONException {
+    public JSONObject(JSONTokener readFrom) throws JSONException {
         /*
          * Getting the parser to populate this could get tricky. Instead, just
          * parse to temporary JSONObject and then steal the data from that.
@@ -174,7 +173,7 @@ public class JSONObject {
      * @throws JSONException if the parse fails or doesn't yield a {@code
      *     JSONObject}.
      */
-    public JSONObject(@NonNull String json) throws JSONException {
+    public JSONObject(String json) throws JSONException {
         this(new JSONTokener(json));
     }
 
@@ -183,7 +182,7 @@ public class JSONObject {
      * from the given object. Names that aren't present in {@code copyFrom} will
      * be skipped.
      */
-    public JSONObject(@NonNull JSONObject copyFrom, @NonNull String @NonNull [] names) throws JSONException {
+    public JSONObject(JSONObject copyFrom, String  [] names) throws JSONException {
         this();
         for (String name : names) {
             Object value = copyFrom.opt(name);
@@ -206,7 +205,7 @@ public class JSONObject {
      *
      * @return this object.
      */
-    @NonNull public JSONObject put(@NonNull String name, boolean value) throws JSONException {
+    public JSONObject put( String name, boolean value) throws JSONException {
         nameValuePairs.put(checkName(name), value);
         return this;
     }
@@ -219,7 +218,7 @@ public class JSONObject {
      *     {@link Double#isInfinite() infinities}.
      * @return this object.
      */
-    @NonNull public JSONObject put(@NonNull String name, double value) throws JSONException {
+    public JSONObject put(String name, double value) throws JSONException {
         nameValuePairs.put(checkName(name), JSON.checkDouble(value));
         return this;
     }
@@ -230,7 +229,7 @@ public class JSONObject {
      *
      * @return this object.
      */
-    @NonNull public JSONObject put(@NonNull String name, int value) throws JSONException {
+    public JSONObject put( String name, int value) throws JSONException {
         nameValuePairs.put(checkName(name), value);
         return this;
     }
@@ -241,7 +240,7 @@ public class JSONObject {
      *
      * @return this object.
      */
-    @NonNull public JSONObject put(@NonNull String name, long value) throws JSONException {
+    public JSONObject put(String name, long value) throws JSONException {
         nameValuePairs.put(checkName(name), value);
         return this;
     }
@@ -257,7 +256,7 @@ public class JSONObject {
      *     infinities}.
      * @return this object.
      */
-    @NonNull public JSONObject put(@NonNull String name, @Nullable Object value) throws JSONException {
+    public JSONObject put( String name, @Nullable Object value) throws JSONException {
         if (value == null) {
             nameValuePairs.remove(name);
             return this;
@@ -274,7 +273,7 @@ public class JSONObject {
      * Equivalent to {@code put(name, value)} when both parameters are non-null;
      * does nothing otherwise.
      */
-    @NonNull public JSONObject putOpt(@Nullable String name, @Nullable Object value) throws JSONException {
+    public JSONObject putOpt(@Nullable String name, @Nullable Object value) throws JSONException {
         if (name == null || value == null) {
             return this;
         }
@@ -301,7 +300,7 @@ public class JSONObject {
      */
     // TODO: Change {@code append) to {@link #append} when append is
     // unhidden.
-    @NonNull public JSONObject accumulate(@NonNull String name, @Nullable Object value) throws JSONException {
+    public JSONObject accumulate( String name, @Nullable Object value) throws JSONException {
         Object current = nameValuePairs.get(checkName(name));
         if (current == null) {
             return put(name, value);
@@ -388,7 +387,7 @@ public class JSONObject {
      *
      * @throws JSONException if no such mapping exists.
      */
-    @NonNull public Object get(@NonNull String name) throws JSONException {
+    public Object get( String name) throws JSONException {
         Object result = nameValuePairs.get(name);
         if (result == null) {
             throw new JSONException("No value for " + name);
@@ -411,7 +410,7 @@ public class JSONObject {
      * @throws JSONException if the mapping doesn't exist or cannot be coerced
      *     to a boolean.
      */
-    public boolean getBoolean(@NonNull String name) throws JSONException {
+    public boolean getBoolean( String name) throws JSONException {
         Object object = get(name);
         Boolean result = JSON.toBoolean(object);
         if (result == null) {
@@ -445,7 +444,7 @@ public class JSONObject {
      * @throws JSONException if the mapping doesn't exist or cannot be coerced
      *     to a double.
      */
-    public double getDouble(@NonNull String name) throws JSONException {
+    public double getDouble( String name) throws JSONException {
         Object object = get(name);
         Double result = JSON.toDouble(object);
         if (result == null) {
@@ -479,7 +478,7 @@ public class JSONObject {
      * @throws JSONException if the mapping doesn't exist or cannot be coerced
      *     to an int.
      */
-    public int getInt(@NonNull String name) throws JSONException {
+    public int getInt( String name) throws JSONException {
         Object object = get(name);
         Integer result = JSON.toInteger(object);
         if (result == null) {
@@ -515,7 +514,7 @@ public class JSONObject {
      * @throws JSONException if the mapping doesn't exist or cannot be coerced
      *     to a long.
      */
-    public long getLong(@NonNull String name) throws JSONException {
+    public long getLong( String name) throws JSONException {
         Object object = get(name);
         Long result = JSON.toLong(object);
         if (result == null) {
@@ -551,7 +550,7 @@ public class JSONObject {
      *
      * @throws JSONException if no such mapping exists.
      */
-    @NonNull public String getString(@NonNull String name) throws JSONException {
+    public String getString( String name) throws JSONException {
         Object object = get(name);
         String result = JSON.toString(object);
         if (result == null) {
@@ -564,7 +563,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists, coercing it if
      * necessary, or the empty string if no such mapping exists.
      */
-    @NonNull public String optString(@Nullable String name) {
+    public String optString(@Nullable String name) {
         return optString(name, "");
     }
 
@@ -572,7 +571,7 @@ public class JSONObject {
      * Returns the value mapped by {@code name} if it exists, coercing it if
      * necessary, or {@code fallback} if no such mapping exists.
      */
-    @NonNull public String optString(@Nullable String name, @NonNull String fallback) {
+    public String optString(@Nullable String name, String fallback) {
         Object object = opt(name);
         String result = JSON.toString(object);
         return result != null ? result : fallback;
@@ -585,7 +584,7 @@ public class JSONObject {
      * @throws JSONException if the mapping doesn't exist or is not a {@code
      *     JSONArray}.
      */
-    @NonNull public JSONArray getJSONArray(@NonNull String name) throws JSONException {
+    public JSONArray getJSONArray(String name) throws JSONException {
         Object object = get(name);
         if (object instanceof JSONArray) {
             return (JSONArray) object;
@@ -610,7 +609,7 @@ public class JSONObject {
      * @throws JSONException if the mapping doesn't exist or is not a {@code
      *     JSONObject}.
      */
-    @NonNull public JSONObject getJSONObject(@NonNull String name) throws JSONException {
+    public JSONObject getJSONObject(String name) throws JSONException {
         Object object = get(name);
         if (object instanceof JSONObject) {
             return (JSONObject) object;
@@ -656,7 +655,7 @@ public class JSONObject {
      * modified after the iterator is returned, the iterator's behavior is
      * undefined. The order of the keys is undefined.
      */
-    @NonNull public Iterator<@NonNull String> keys() {
+    public Iterator<String> keys() {
         return nameValuePairs.keySet().iterator();
     }
 
@@ -689,7 +688,7 @@ public class JSONObject {
      * Encodes this object as a compact JSON string, such as:
      * <pre>{"query":"Pizza","locations":[94043,90210]}</pre>
      */
-    @Override @NonNull public String toString() {
+    @Override public String toString() {
         try {
             JSONStringer stringer = new JSONStringer();
             writeTo(stringer);
@@ -714,7 +713,7 @@ public class JSONObject {
      * @param indentSpaces the number of spaces to indent for each level of
      *     nesting.
      */
-    @NonNull public String toString(int indentSpaces) throws JSONException {
+    public String toString(int indentSpaces) throws JSONException {
         JSONStringer stringer = new JSONStringer(indentSpaces);
         writeTo(stringer);
         return stringer.toString();
@@ -734,7 +733,7 @@ public class JSONObject {
      * @param number a finite value. May not be {@link Double#isNaN() NaNs} or
      *     {@link Double#isInfinite() infinities}.
      */
-    @NonNull public static String numberToString(@NonNull Number number) throws JSONException {
+    public static String numberToString(Number number) throws JSONException {
         if (number == null) {
             throw new JSONException("Number must be non-null");
         }
@@ -762,7 +761,7 @@ public class JSONObject {
      * @param data the string to encode. Null will be interpreted as an empty
      *     string.
      */
-    @NonNull public static String quote(@Nullable String data) {
+    public static String quote(@Nullable String data) {
         if (data == null) {
             return "\"\"";
         }
